# Standard libraries
import json
import requests

# Django
from django.conf import settings

# DRF
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

class HubSpotBlogAPIView(APIView):
  ''' HubSpot API '''

  def get(self, request, format=None):
    ''' Retrieve a Blog from HubSpot '''

    API_KEY = settings.HUBSPOT_API_KEY #os.environ.get('HUBSPOT_API_KEY')
    URL = f'https://api.hubapi.com/content/api/v2/blog-posts/?hapikey={API_KEY}'

    try:
      blog = requests.get(URL)
      return Response(json.loads(blog.text), status=status.HTTP_200_OK)

    except Exception as e:
      print(e)
      return Response(str(e), status=status.HTTP_400_BAD_REQUEST)