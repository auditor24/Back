# AWS
import boto3


def search(text, **kwargs):
    ''' Handle the search for items in AWS-S3 buckets '''

    # Execution of Kendra's query method
    kendra = boto3.client("kendra")
    kendra_index_id = "a82cbfe1-0e23-4aec-a325-fa3c12af2ddd"
    kendra_response = kendra.query(
        IndexId = kendra_index_id,
        QueryText = text,
        **kwargs
    )

    # Grouping of relevant information from the search results
    results = []
    for item in kendra_response['ResultItems']:
        results.append({
            'title': item['DocumentTitle']['Text'],
            'relevance': item['ScoreAttributes']['ScoreConfidence'],
            'url': item['DocumentURI'].replace(' ','+'),
            # 'dummy_url': item['DocumentURI'].replace(' ','+'),
            # 'excerpt': item['DocumentExcerpt']['Text']
        })
    return results