from os import path
import os

from unittest import mock

from django_mock_queries.query import MockSet
from django.test import Client, TestCase

from mongo import mongodb
from pymongo import MongoClient


# Running per one --> pytest -vv tests/test_crudmongo.py

# ----------------------------||------------------
# Test State --->  5 passed, 26 warnings in 33.13s
# ----------------------------||------------------

# Run all tests  --> pytest


# class TestConnectionDB:
#     def test_connection(self):
#         try:
#             db_handle, client = mongodb.get_db_handle()
#             client.server_info()
#         except BaseException as e:
#             raise e


class TestMongoDBCRUD:

    @mock.patch.object(mongodb, 'insert')
    def test_mongo_insert(self, mock_insert):
        mock_insert.return_value = "6179b5024d6eb21e101a3920"
        doc = {"test": "monguito"}
        collec = "test"
        res = mongodb.insert(collection=collec, document=doc)
        print(res)
        assert type(res) == str
        assert mock_insert.return_value == res


    @mock.patch("mongo.mongodb.find")
    def test_mongo_find(self, mock_find):
        mock_find.return_value = '{"_id": {"$oid": "6179b5024d6eb21e101a3920"}, "test": "monguito", "created_at": "27/10/2021 20:22:26"}'
        collec = "test"
        res = mongodb.find(collection=collec, id=id)
        print(res)
        assert type(res) == str
        assert mock_find.return_value == res


    @mock.patch("mongo.mongodb.update")
    def test_mongo_update(self, mock_update):
        mock_update.return_value = '<pymongo.results.UpdateResult object at 0x103141e10>'
        doc = {"test": "monguito"}
        collec = "test"
        query_filter = {"A": "AAA"}
        res = mongodb.update(query_filter=query_filter, collection=collec, document=doc)
        print(res)
        assert type(res) == str
        assert mock_update.return_value == res

    @mock.patch("mongo.mongodb.delete")
    def test_mongo_delete(self, mock_delete):
        mock_delete.return_value = '<pymongo.results.DeleteResult object at 0x110be8780>'
        collec = "test"
        query_filter = {"test": "mongo"}
        res = mongodb.delete(query_filter=query_filter, collection=collec)
        print(res)
        assert type(res) == str
        assert mock_delete.return_value == res

    # def test_results_delete(self):
    #     return_value = '<pymongo.results.DeleteResult object at 0x110be8780>'
    #     collec = "test"
    #     query_filter = {"test": "mongo"}
    #     res = mongodb.delete(query_filter=query_filter, collection=collec)
    #     print(res)
    #     assert type(res) == str

    # def test_results_insert(self):
    #     return_value = "6179b5024d6eb21e101a3920"
    #     doc = {"test": "monguito"}
    #     collec = "test"
    #     res = mongodb.insert(collection=collec, document=doc)
    #     print(res)
    #     assert type(res) == str