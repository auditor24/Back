# Standard Libraries
import json
import requests

# Unittest Libraries
import pytest
from unittest import mock
from django.test import Client, TestCase
from django.test.client import RequestFactory

# Django
from django.conf import settings
from django.http import HttpRequest
from django_mock_queries.query import MockSet
from django.core.handlers.base import BaseHandler
from django.http.request import QueryDict

# DRF
from rest_framework import status
from rest_framework.test import APITestCase, APIRequestFactory, APIClient

# Mongo DB
from mongo import mongodb
from pymongo import MongoClient

# Serializers
from iota.market import serializers
from iota.market.serializers.services import AlchemerSerializer, QuickSurveySerializer

# Models
from iota.market.views.services import AlchemerSurvey, CintFunctionalities


# Running per one --> pytest -vv tests/test_quicksurvey.py

# #########################################################
# Test State --->  5 passed, 6 warnings in 11.63s
# ---------------------------------------------------------
# Total Tests ----> 5 TestCases
# #########################################################

# Run all tests  --> pytest


class TestQuickSurveySerializers(TestCase):
    """
    Class for Test Quick Survey Serializers, output return success.
    """

    def setUp(self):
        self.serializer = QuickSurveySerializer

    def test_user_model_serializer(self):
        data = {
                "price": None,
                "completes": 124,
                "education_quota": [3760, 3761, 3762, 3763, 3764],
                "gender_quota": {
                    "name": "Gender",
                    "quotas": [
                        {
                            "limit": 124,
                            "name": "Male",
                            "targetGroup": {
                                "gender": 1
                            }
                        },
                        {
                            "limit": 124,
                            "name": "Female",
                            "targetGroup": {
                                "gender": 2
                            }
                        }
                    ]
                },
                "incidence_field": 80,
                "income_quota": [7996, 7997, 7998, 7999],
                "interview_length": 7,
                "max_age": 65,
                "min_age": 34,
                "states": [],
                "template_url": "https://survey.alchemer.com/s3/6717954/Awareness-V2-IICS41?CintGUID=[ID]"
        }

        serializer = self.serializer(data=data)
        assert serializer.is_valid(raise_exception=True)
        assert serializer.validated_data == data
        assert serializer.data == data
        assert serializer.errors == {}


class TestCintFunctionalities(TestCase):
    """
    Class for Test Cint Endpoints and requests.
    """

    def setUp(self):

        self.client = APIClient()
        self.order = "1226344"
        # create_survey = CreateCintSurvey.send_survey(self, self.order)

    # def test_stop_survey_action(self):

    #     data = {"cint_id": self.order}

    #     request =  self.client.post(
    #         settings.BACK_URL + '/market-api/services/cint/stop_surveys/',
    #         json.dumps(data),
    #         content_type='application/json'
    #     )
    #     self.assertTrue(request.status_code, 204)

    # def test_play_survey_action(self):

    #     # response = CintFunctionalities.play_surveys(self, self.order)
    #     # self.assertTrue(response.status_code, 204)
    #     # response_stop = CintFunctionalities.stop_surveys(self, self.order)
    #     # self.assertTrue(response_stop.status_code, 204)

    #     data = {"cint_id": self.order}

    #     response =  self.client.post(
    #         settings.BACK_URL + '/market-api/services/cint/play_surveys/',
    #         json.dumps(data),
    #         content_type='application/json'
    #     )
    #     self.assertTrue(response.status_code, 204)


    # def test_states_function(self):

    #     data = { "states": ["North Carolina","New York"] }

    #     # data = { "states": [] }

    #     request =  self.client.post(
    #         settings.BACK_URL + '/market-api/services/cint/states/',
    #         json.dumps(data),
    #         content_type='application/json'
    #     )

    #     request = json.loads(request.content.decode())
    #     self.assertTrue(request[0], [481764, 481738])


# @pytest.mark.django_db
# class TestAlchemerFunctionalities:
#     """
#     Class for Test all Alchemer Endpoints and requests to other API's
#     """

#     def test_alchemer_id_return(self):

#         client = APIClient()

#         data = {'survey_id': '6703283'}

#         request =  client.post(
#             settings.BACK_URL + '/market-api/services/alchemer/alchemer_id/',
#             json.dumps(data),
#             content_type='application/json'
#         )

#         assert request.status_code == 200
#         assert not request.data == None
#         assert not request.data['data'] == None
