# Standard Libraries
import pytest
import json
import requests

# Unittest Libraries
from unittest import mock

# Django
from django.conf import settings
from django.test import Client, TestCase
from django_mock_queries.query import MockSet

# DRF
from rest_framework import status
from rest_framework.test import APITestCase

# Mongo DB
from mongo import mongodb
from pymongo import MongoClient

# Serializers
from iota.users.serializers import UserModelSerializer


# Running per one --> pytest -vv tests/test_authentication.py

# #########################################################
# Test State ---> 2 failed, 7 passed, 2 warnings in 12.17s
# ---------------------------------------------------------
# Total Tests ----> 9 Tests Cases
# #########################################################

# Run all tests  --> pytest


class TestApiUserViews:
    """
    Class to test Api views.
    """

    def test_freelancers_views(self):
        try:
            response = requests.get(settings.BACK_URL + "/users-api/freelancers/")

        except requests.exceptions.RequestException as error:
            raise SystemExit(error)

        assert response.status_code == 200

    def test_agencies_views(self):
        try:
            response = requests.get(settings.BACK_URL + "/users-api/agencies/")

        except requests.exceptions.RequestException as error:
            raise SystemExit(error)

        assert response.status_code == 200

    def test_login_view(self):
        try:
            response = requests.get(settings.BACK_URL + "/users-api/users/login/")

        except requests.exceptions.RequestException as error:
            raise SystemExit(error)

        assert response.status_code == 405


class TestSecurityFailed:

    """
    Class for test security of Api.
    All test function should not be failed or the Api would be vulnerable
    """

    def test_users_with_no_credentials(self):
        try:
            response = requests.get(settings.BACK_URL + "/users-api/users/")

        except requests.exceptions.RequestException as error:
            raise SystemExit(error)

        assert response.status_code == 401

    def test_market_with_false_credentials(self):

        headers = { "Authorization": "Bearer " + 'eyJ0eXAiOiJKV2tir4'}
        try:
            response = requests.get(settings.BACK_URL + "/market-api/details/", headers=headers)

        except requests.exceptions.RequestException as error:
            raise SystemExit(error)

        assert response.status_code == 401


class TestCredentials:

    def test_login_user(self):
        try:
            data = {
            "username": "dev_1",
            "password": "dev_1"
            }
            response = requests.post(settings.BACK_URL + "/users-api/users/login/", data=data)

        except requests.exceptions.RequestException as error:
            raise SystemExit(error)

        assert response.status_code == 200


class TestUserPath(TestCase):

    # Setup function to get Authentication Credentials
    def setUp(self):

        # Login
        data = {
            "username": "dev_1",
            "password": "dev_1"
        }
        response = requests.post(settings.BACK_URL + "/users-api/users/login/", data=data)

        response = json.loads(response.content.decode())
        key = response["access"]
        self.headers = { "Authorization": "Bearer " + key }


    def test_market_api_view(self):

        try:
            response = requests.get(settings.BACK_URL + "/market-api/details/", headers=self.headers)

        except requests.exceptions.RequestException as error:
            raise SystemExit(error)

        self.assertTrue(response.status_code, 200)


class TestUserSerializers(TestCase):
    """
    Class for Test User Serializers, output return success.
    """

    def setUp(self):
        self.serializer = UserModelSerializer

    def test_user_model_serializer(self):
        data = {
                'role': None,
                'username': 'test_dev',
                'email': 'test@gmail.com',
        }

        serializer = self.serializer(data=data)
        assert serializer.is_valid(raise_exception=True)
        assert serializer.validated_data == data
        assert serializer.data == data
        assert serializer.errors == {}

    def test_invalid_user_serializer(self):
        serializer = self.serializer(data={'role': None, 'username': 'test_dev', 'password': 'test', 'password_confirmation': 'tests'})
        assert not serializer.is_valid()
        assert serializer.validated_data == {}
        assert serializer.data == {'role': None, 'username': 'test_dev'}
        assert serializer.errors == {'email': ['This field is required.']}

