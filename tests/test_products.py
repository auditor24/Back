# Standard Libraries
from urllib import response
import pytest
import json
import requests

# Unittest Libraries
from unittest import mock

# Django
from django.conf import settings
from django.test import Client, TestCase
from django_mock_queries.query import MockSet

# DRF
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from iota.market import serializers
from iota.market.serializers.shopping_cart_items import ShoppingCartItemModelSerializer

# Mongo DB
from mongo import mongodb
from pymongo import MongoClient

# Serializers
from iota.market.serializers import ProductModelSerializer
from iota.market.serializers.products import DummyProductModelSerializer, ProductDetailModelSerializer, ShoppingCartProductDetailModelSerializer

# Running per one --> pytest -vv tests/test_products.py

# #########################################################
# Test State ---> 4 passed, 2 warnings in 5.64s
# ---------------------------------------------------------
# Total Tests ----> 4 Tests Cases
# #########################################################

# Run all tests  --> pytest

class TestApiProductsView:
    '''
    Class test for products URL working. Success if response is 200
    '''
    def test_products_view(self):
        try:
            response = requests.get(f'{settings.BACK_URL}/market-api/products')
        except requests.exceptions.RequestException as error:
            raise SystemExit(error)

        assert response.status_code == 200

# class TestProductSerializer(TestCase):
#     '''
#     Test Case for validating serialized data for Products, dummy products data and invalid data.
#     '''
#     def setUp(self):
#         self.serializer = ProductModelSerializer
#         self.serializerdummy = DummyProductModelSerializer
#         self.detailserializer = ProductDetailModelSerializer
#         self.shopcartproductdetail = ShoppingCartProductDetailModelSerializer
        

#     def test_products_detail_model_serializer(self):

#         data = {
#             "price": 40.00,
#             "discount": 5.00,
#             "tax": 2.00,
#             "belongs_to": "product",
#             "type": "research_report",
#             "description": "Product Test Description",
#             "delivery_time": 20,
#             "sample_url": "https://sample_url.com",
#         }

#         serializer = self.serializer(data=data)
#         assert serializer.is_valid(raise_exception=True)
#         assert serializer.validated_data == data
#         assert serializer.data == data
#         assert serializer.errors == {}


#     def test_products_models_serializer(self):
        
#         data = {
#             "title" : "Test Title",
#             "url" : "https://testurl.com",
#         }

#         serializer = self.serializer(data=data)
#         assert serializer.is_valid(raise_exception=True)
#         assert serializer.validated_data == data
#         assert serializer.data == data
#         assert serializer.errors == {}

#     def test_invalid_data_type(self):

#         data = {
#             "title" : "Test Title",
#             "url" : "https://testurl.com"
#         }

#         serializer = self.serializer(data=[data])
#         assert not serializer.is_valid()
#         assert serializer.validated_data == {}
#         assert serializer.data == {}
#         assert serializer.errors == {'non_field_errors': ['Invalid data. Expected a dictionary, but got list.']}

#     def test_dummy_products(self):

#         data = {
#             "title": "Test Title",
#             "dummy_url": "https://dummyurl.com"
#         }

#         serializer = self.serializerdummy(data=data)
#         assert serializer.is_valid(raise_exception=True)
#         assert serializer.validated_data == data
#         assert serializer.data == data
#         assert serializer.errors == {}


# class TestProductAccess(TestCase):

#     def setUp(self):

#         self.client = APIClient()
#         # Login
#         data = {
#             "username": "dev_1",
#             "password": "dev_1"
#         }
#         response = self.client.post(settings.BACK_URL + "/users-api/users/login/", data=data)

#         response = json.loads(response.content.decode())
#         key = response["access"]
#         self.headers = { "Authorization": "Bearer " + key }

#     # TODO: def test_product_purchase(self):
#     #     pass


#     def test_list_products(self):
#         response = self.client.get(settings.BACK_URL + "/market-api/users/my-space/", headers=self.headers)
#         assert response.status_code == 200