# Django
from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static

# DRF
from rest_framework import permissions
from rest_framework_swagger.views import get_swagger_view

# Swagger
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

# Views
from iota.market.views import HomeTemplateView
from iota.celery.views import CeleryDebugAPIView
from iota.wizard.views import industries_l1_l2, industries_l2_l3
from iota.hubspot.views import HubSpotBlogAPIView


# schema_view = get_swagger_view(title="EMS API Documentation")
schema_view = get_schema_view(
    openapi.Info(
        title="Backend APIREST",
        default_version='api',
        description="Iota Backend",
        terms_of_service="https://www.iotaq.tech.com",
        contact=openapi.Contact(email="data@iotaimpact.com"),
        license=openapi.License(name="Test License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    # Admin
    path(settings.ADMIN_URL, admin.site.urls),
    # Home
    path('', HomeTemplateView.as_view(), name='home'),

    # APIs
    path('users-api/', include(('iota.users.urls', 'users'), namespace='users-api')),
    path('market-api/', include(('iota.market.urls', 'market'), namespace='market-api')),

    # Simple Endpoints
    # path('wizard/', include(('iota.wizard.urls', 'market'), namespace='market-api')),
    path('l2/<str:le1>', industries_l1_l2, name='industries-level-2'),
    path('l3/<str:le2>', industries_l2_l3, name='industries-level-3'),
    path('blogs/', HubSpotBlogAPIView.as_view(), name='blogs'),

    # Celery
    path('celery-debug/', CeleryDebugAPIView.as_view()),

    # Swagger
    path(
        '^doc(?P<format>\.json|\.yaml)$',
        schema_view.without_ui(cache_timeout=0),
        name='schema-json'
    ),
    path(
        'doc/',
        schema_view.with_ui('swagger', cache_timeout=0),
        name='schema-swagger-ui'
    ),
    path(
        'redoc/',
        schema_view.with_ui('redoc', cache_timeout=0),
        name='schema-redoc'
    )
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)