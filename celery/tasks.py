'''
Shared Task Decorator allows to create tasks without having any concrete app instance:
https://docs.celeryproject.org/en/stable/django/first-steps-with-django.html#using-the-shared-task-decorator
'''
# Django
from django.utils import timezone

# Celery
from celery import shared_task


@shared_task()
def add(x, y):
    return x + y

@shared_task()
def scheduled_task():
    return timezone.now()

@shared_task()
def part_A():
    return {'a': 'A', 'b': 'B'}

@shared_task()
def part_B(*args, **kwargs):
    args[0]
    return 'ok'