Celery useful commands

Workers:

    Command to start the worker:
        Linux (not tested):
            celery -A iota worker -l INFO
        Windows:
            celery -A iota worker -l INFO --pool=solo
            celery -A iota worker -l INFO --pool=solo -E
            celery -A iota worker -l INFO --pool=solo -E -Q queue_name
            celery -A iota worker -l INFO --pool=solo --autoscale=4,2

    https://docs.celeryproject.org/en/master/getting-started/next-steps.html#remote-control
        celery -A iota inspect active
        celery -A iota control enable_events | disable_events

Tasks:
    https://docs.celeryproject.org/en/stable/userguide/tasks.html

Monitoring with Flower (default port=5555):
    celery -A iota flower -Q debug_queue
    flower -A iota -Q foo
    celery -A proj flower --port=user_defined_port
    celery flower --broker=redis://development:ETUd94rhsW@QbUB@redis-12755.c277.us-east-1-3.ec2.cloud.redislabs.com:12755 --broker_api=http://localhost:5555/broker