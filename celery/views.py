'''
Views for testing Celery functionalities.
Read the docs before testing:
    + https://docs.celeryproject.org/en/master/userguide/calling.html
    + https://docs.celeryproject.org/en/master/userguide/canvas.html
'''
# Standard libraries
# from datetime import timedelta

# Django
# from django.utils import timezone

# DRF
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

# Celery
# from celery import chain

# Tasks
# from .tasks import add, scheduled_task, part_A, part_B


class CeleryDebugAPIView(APIView):
    '''
    APIView for testing celery functionalities.
    Check the GET method and comment/uncomment the desired
    functionalitites that you want too test.
    '''

    def get(self, request, format=None):
        ''' Linking callbacks '''
        # add.apply_async((2, 2), link=add.s(16))

        '''
        Scheduled tasks with ETA and countdown options:
            + ETA (Estimated time of arrival): Date and time
            + Countdown: seconds in the future
        '''
        # now = timezone.now()
        # eta = now + timedelta(minutes=1)
        # print(f'Scheduled at: {timezone.now()}. \nTo be executed in {eta} minutes')
        # scheduled_task.apply_async(eta=eta)

        ''' Signatures and methods to call them '''
        # add.s(2, 2)() # Inmediate
        # signature = add.s(2, 2) # Delay
        # signature.delay()
        # signature.apply_async(countdown=1) # apply_async with kwargs

        # Linking tasks passing the result of the parent to the child
        # signature = part_A.s()
        # signature.link(part_B.s(to='santiago'))
        # signature.delay()

        ''' Chaining tasks with signatures (chain) '''
        # chain(add.s(2, 2), add.s(4), add.s(8))()

        ''' Chaining inmutable tasks with signatures (pipes) '''
        # (add.si(2, 2) | add.si(4, 4) | add.si(8, 8))()

        return Response('ok', status=status.HTTP_200_OK)