'''
Predesigned filters to search for objects
in amazon S3 using the kendra search engine
'''

reports = {
    "AndAllFilters": [{
        "NotFilter": {
            "EqualsTo": {
                "Key": "_category",
                "Value": { "StringValue": "dashboard" }
            }
        }
    },{
        "NotFilter": {
            "EqualsTo": {
                "Key": "_category",
                "Value": { "StringValue": "partner" }
            }
        }
    }],
}


dashboards = {
    "EqualsTo": {
        "Key": "_category",
        "Value": {
            "StringValue": "dashboard"
        }
    }
}


partners = {
    "EqualsTo": {
        "Key": "_category",
        "Value": {
            "StringValue": "partner"
        }
    }
}

