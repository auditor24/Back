def one_word_format(list_items):
    ''' Set the oneword format to each string item of a list '''
    return [value.lower().replace(' ', '').replace(',', '') for value in list_items]