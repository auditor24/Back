def get_attribute_value(kendra_item, key):
    attributes = kendra_item['DocumentAttributes']
    value = [item['Value']['StringValue'] for item in attributes if item['Key']==key]
    return value[0]