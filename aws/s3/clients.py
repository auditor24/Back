# Standard libraries
from pathlib import Path

# Django
from storages.backends.s3boto3 import S3Boto3Storage


class ReportsStorage(S3Boto3Storage):
    bucket_name = 'iotatech-kendra.bucket'
    location = 'reports'

    def save(name, content, max_length=None):
        return super().save(name, content, max_length=None)


class DashboardsStorage(S3Boto3Storage):
    bucket_name = 'iotatech-kendra.bucket'
    location = 'reports'