FROM python:3.9-bullseye
ENV PYTHONUNBUFFERED 1
# Install and configure google_stable for Debian
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt-get update
RUN apt install -y ./google-chrome-stable_current_amd64.deb
# Setting Working directory.
WORKDIR /iotaq-app
ADD . .
RUN pip install --upgrade pip && pip install -r celery.requirements.txt
CMD ["celery", "-A", "iota", "worker", "-l", "INFO"]