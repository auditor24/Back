# from environs import Env
import os
from dotenv import load_dotenv

load_dotenv()

# env = Env()
# env.read_env()

# Mongodb
from pymongo import MongoClient, ASCENDING, DESCENDING
from bson.json_util import dumps, ObjectId

# Mongodb - GridFS
from mongo.mongodb_gridfs import gridfs_put

# TLS
import certifi

# Utils
from iota.utils.dates_management import add_created_at

def get_db_handle(**kwargs):
    ''' Database connector
    Optional Arguments:
        host, username, password, db_name
    Return:
        client: Mongo Client
        db_handle: client[db_name]
    '''

    host = kwargs.get('host', os.environ.get('MONGO_ATLAS_HOST'))
    username = kwargs.get('username', os.environ.get('MONGO_ATLAS_USERNAME'))
    password = kwargs.get('password', os.environ.get('MONGO_ATLAS_PASSWORD'))
    db_name = kwargs.get('db_name', 'iotaimpact')
    
    retry = kwargs.get('retry', 0)
    try:
        url = f"mongodb+srv://{username}:{password}@{host}/{db_name}?retryWrites=true&w=majority"
        client = MongoClient(url, tlsCAFile=certifi.where())

        db_handle = client[db_name]
        return db_handle, client

    except Exception as e:
        retry += 1
        print(f'Something went wrong, attempt {retry} to re-connect to Mongodb')

        if retry < 3:
            return get_db_handle(retry=retry)
        else:
            raise Exception(f'Can not connect with Mongodb, contact your DBA.\nError: {e}')

def insert(**kwargs):
    ''' General Purpose INSERT
    Arguments: 
        db_name (optional): target database (default: iotaimpact)
        collection: target collection
        document(s): Object or List of Objects to be stored
    Return: 
        ID(s) of the inserted object(s)
    '''
    db_name = kwargs.get('db_name', 'iotaimpact')
    db, _ = get_db_handle(db_name=db_name)

    collection = kwargs.get('collection', 'undefined_collection')
    document = kwargs.get('document', None)
    documents = kwargs.get('documents', None)
    
    # insert_one
    if document:
        add_created_at(document)
        return str(db[collection].insert_one(document).inserted_id)

    # insert_many
    elif documents:
        return str(db[collection].insert_many(documents))

def update(**kwargs):
    ''' Insert one or more fields into one or all documents in a collection
    Example:
        collection="test"
        document = {"new_field": 'new_value'}
        query_filter = {"test": "mongo"}
        
    Required Arguments:
        collection: target collection
        document: dictionary with the new fields
        query_filter: specify a filter if you want to update just one document

    Optional Arguments:
        database credentials
        amount: "all" (default: "one")
    '''

    db_name = kwargs.get('db_name', 'iotaimpact')
    db, _ = get_db_handle(db_name=db_name)

    query_filter = kwargs.get('query_filter', None)
    collection = kwargs.get('collection', None)
    document = kwargs.get('document', None)
    amount = kwargs.get('amount', 'one')

    if collection is None or query_filter is None or document is None:
        raise Exception('Specify collection, query_filter and document')

    # if amount == 'all':
    #     # Return the count of documents updated (count: integer)
    #     return db[collection].update_many(query_filter, {'$set': document}).modified_count    
    if amount == 'all':
        return str(db[collection].update_many(query_filter, {'$set': document}))
    
    # else:
    #     # return the updated object (type: dict)
    #     sort = kwargs.get('sort', 'DESC')
    #     sort = DESCENDING if sort=='DESC' else ASCENDING
    #     return db[collection].find_one_and_update(query_filter, {'$set': document}, sort=[("_id", sort)])
    else:
        sort = kwargs.get('sort', 'DESC')
        sort = DESCENDING if sort=='DESC' else ASCENDING
        return str(db[collection].find_one_and_update(query_filter, {'$set': document}, sort=[("_id", sort)] ))
        

def find(**kwargs):
    ''' General Purpose FIND 
    Example:
        collection='surveys'
        query_filter={"user_token": "181.61.59.165:2021-10-26"}
        projection={"highest_match_report": 1, "paid":1, "created_at": 1}
        skip=30 (Default: 0)
        limit=50
        sort='ASC' (Default: 'DESC')
        find(collection=collection, query_filter=query_filter, projection=projection, limit=limit)
        
    Arguments: 
        - Required
        db_name (optional): target database
        collection: target collection

        - Search types (Choose One)
        id: find by ID
        query_filter: find with filter

        - Options
        projection: retrieve specified fields
        skip & limit: pagination
        sort: sort ASC or DESC by ID

    Return: 
        list of documents found
    '''
    db_name = kwargs.get('db_name', 'iotaimpact')
    db, _ = get_db_handle(db_name=db_name)

    collection = kwargs.get('collection', None)
    if collection is None:
        raise Exception('Specify the collection')

    # Find by ID
    id = kwargs.get('id', None) 
    
    # Query
    query_filter = kwargs.get('query_filter', None)
    
    # Projection
    projection = kwargs.get('projection', None)
    
    # Sort
    sort = kwargs.get('sort', 'DESC')
    sort = DESCENDING if sort=='DESC' else ASCENDING
    
    # Skip and Limit
    skip = kwargs.get('skip', 0)
    limit = kwargs.get('limit', None)
    
    search_result = None
    
    # Find by ID
    if id:
        search_result = dumps(db[collection].find_one({"_id": ObjectId(id)}, projection))

    # Find by filter
    elif query_filter: 
        
        if limit is not None:
            search_result = dumps(db[collection].find(filter=query_filter, projection=projection, skip=int(skip), limit=int(limit), sort=[("_id", sort)]))
        else:
            search_result = dumps(db[collection].find(filter=query_filter, projection=projection, skip=int(skip), sort=[("_id", sort)]))
            
    return search_result

def delete(**kwargs):
    ''' Delete one or all documents in a collection
    Example:
        collection="test"
        query_filter = {"test": "mongo"}
        
    Required Arguments:
        collection: target collection
        query_filter: specify a filter if you want to update just one document

    Optional Arguments:
        database credentials
        amount: "all" (default: "one")
    '''

    db_name = kwargs.get('db_name', 'iotaimpact')
    db, _ = get_db_handle(db_name=db_name)

    collection = kwargs.get('collection', None)
    query_filter = kwargs.get('query_filter', None)
    amount = kwargs.get('amount', 'one')

    if amount == 'all':
        return str(db[collection].delete_many(query_filter))
    
    else:
        return str(db[collection].delete_one(query_filter))

# GridFS Methods
def insert_large_document(binary_document, **kwargs):
    ''' Allows storage of documents larger than 16 MB using GridFS 
    Minimal Arguments:
        insert_large_document(document, filename=filename)
    '''

    # Document validation
    if not binary_document:
        raise Exception('Specify the document to save')

    # Get the database client
    db_name = kwargs.get('db_name', 'iotaimpact')
    db, _ = get_db_handle(db_name=db_name)

    # Mongodb - GridFS storage
    inserted_id = gridfs_put(db, binary_document, **kwargs)
    return str(inserted_id)