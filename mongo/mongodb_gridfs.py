''' 
GridFS is a Mongo Tool for storing and retrieving 
files that exceed the Mongodb's BSON-document 
size limit of 16 MB.
'''

# OS
from os import path

# Mongodb - GridFS
import gridfs

def gridfs_put(db, binary_document, **kwargs):
    ''' It is the equivalent of creating a new file and writing on it '''

    # The entry of a document or its path is validated.
    document_type = type(binary_document)
    if document_type is not str and document_type is not bytes:
        raise Exception('Currently this GridFS function just accept string content or a path to a file')

    # GridFS Storage
    fs = gridfs.GridFS(db, disable_md5=True)
    inserted_id = None
    
    # File Storage
    if path.isfile(binary_document):
        inserted_id = fs.put(open(binary_document, 'rb'), **kwargs)

    # String Storage
    elif document_type is str:
        # Encodes the content if it is not in byte format
        if document_type is not bytes:
            binary_document = binary_document.encode()

        inserted_id = fs.put(binary_document, **kwargs)
    
    return inserted_id




