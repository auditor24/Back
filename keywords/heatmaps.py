'''Create heatmap of grow and decline words search'''


import pandas as pd
import statsmodels.api as sm # Import Statsmodels to build SARIMA
from statistics import mean
import plotly.express as px
import kaleido
import dataframe_image as dfi
import os
from io import BytesIO

def trend_change_df(wordlist, interest_over_time_volume, datelist, parameter, keyword_df):
    """
    Wordlist is the list from the word of Keyword_Everyword list; Interest_over_time_volume is the dataframe contained on converted_updated;
    Finally, datelist is a list with 4 date elements (period1_start, period2_end, period2_start, period2_end). This elements can be formated as %Y OR %Y-%m.
    This function creates a dataframe of heatmaps with information of trend change over a period defined in datelist variable.
    """

    keyword_volume = keyword_df
    keyword_cpc = keyword_volume.loc[:, ['Keyword', 'CPC (US)']]
    period1_coef = []
    period1_change = []
    period2_coef = []
    period2_change = []
    wordlist = pd.DataFrame(interest_over_time_volume.columns)
    wordlist.rename(columns = {0: 'Keyword'}, inplace=True)
    keyword_plus_cpc = pd.merge(wordlist, keyword_cpc[['Keyword', 'CPC (US)']], on='Keyword', how="inner")
    for i in keyword_plus_cpc['Keyword']:
        print(i)
        if interest_over_time_volume[i].isnull().values.any():
            interest_over_time_volume[i] = interest_over_time_volume[i].fillna(interest_over_time_volume[i].mean())
        decomposition = sm.tsa.seasonal_decompose(interest_over_time_volume[i], model='additive', extrapolate_trend=1, two_sided=True)
        trend_data = decomposition.trend
        period1_data = trend_data.loc[datelist[0] : datelist[1]] #The idea is to filter the data for year 2018-2019 and make the analysis. Don't know YET how to do this.
        try:
            period1_result = round(mean(period1_data[(int((len(period1_data)+1)/2)):]) / mean(period1_data[:(int((len(period1_data)+1)/2))]) - 1, 2)
        except ZeroDivisionError:
            print('ZeroDivisionError: Data for ' + i + ' is scarce')
            period1_coef.append('NaN')
            period1_change.append('NaN')
        else:
            period1_coef.append(period1_result)
            if period1_result <= parameter[0]:
                period1_change.append('Significant decline')
            elif period1_result <= parameter[1]:
                period1_change.append('Moderate decline')
            elif period1_result < parameter[2]:
                period1_change.append('Stable')
            elif period1_result < parameter[3]:
                period1_change.append('Moderate growth')
            elif period1_result >= parameter[3]:
                period1_change.append('Significant growth')
        
        period2_data = trend_data.loc[datelist[2] : datelist[3]] #The idea is to filter the data for year 2020-now and make the analysis. Don't know YET how to do this.
        try:
            period2_result = round(mean(period2_data[(int((len(period2_data)+1)/2)):]) / mean(period2_data[:(int((len(period2_data)+1)/2))]) - 1, 2)
        except ZeroDivisionError:
            print('ZeroDivisionError: Data for period 2 of ' + i + ' is scarce')
            period2_coef.append('NaN')
            period2_change.append('NaN')
        else:
            period2_coef.append(period2_result)
            if period2_result <= parameter[0]:
                period2_change.append('Significant decline')
            elif period2_result <= parameter[1]:
                period2_change.append('Moderate decline')
            elif period2_result < parameter[2]:
                period2_change.append('Stable')
            elif period2_result < parameter[3]:
                period2_change.append('Moderate growth')
            elif period2_result >= parameter[3]:
                period2_change.append('Significant growth')
    coef1 = str(datelist[0] + ' to ' + datelist[1] + ' coef')
    change1 = str(datelist[0] + ' to ' + datelist[1] + ' change')
    coef2 = str(datelist[2] + ' to ' + datelist[3] + ' coef')
    change2 = str(datelist[2] + ' to ' + datelist[3] + ' change')
    dictionary = {'Keyword': keyword_plus_cpc['Keyword'],
              'CPC (US)': keyword_plus_cpc['CPC (US)'],
              coef1 : period1_coef,
              change1 : period1_change,
              coef2 : period2_coef, 
              change2 : period2_change}
    heatmap = pd.DataFrame(dictionary)
    return heatmap, wordlist

def background_color(val):
	"""Add colors to the heatmap"""

	if val == "Significant decline":
		color = '#FF0000'
	elif val == "Moderate decline":
		color = '#FCE4D6'
	elif val == "Stable":
		color = '#FEFCA6'
	elif val == "Moderate growth":
		color = '#B4FABB'
	elif val == "Significant growth":
		color = '#00B050'
	else:
		color = 'white'
	return 'background-color: %s' % color

def heatmap_img(heatmap_df):
    """return heatmap image to be used in create ppt"""

    image_heatmap = BytesIO()
    dfi.export(heatmap_df.style.applymap(background_color), image_heatmap)
    return image_heatmap

def state_trend_data(request_dict, i, datelist):
    """return trends by state"""
    
    state_code = []
    state_name = []
    period1_coef = []
    period2_coef = []
    state_dict = {'US-MA': 'Massachusetts',
                  'US-CA': 'California',
                  'US-WA': 'Washington',
                  'US-CO': 'Colorado',
                  'US-ID': 'Idaho',
                  'US-DE': 'Delaware',
                  'US-CT': 'Connecticut',
                  'US-UT': 'Utah',
                  'US-GA': 'Georgia',
                  'US-RI': 'Rhode Island'}
    for code, name in state_dict.items():
        try:
            interest_over_time_temp = request_dict[code]['interest_over_time_volume'][i]
        except KeyError:
            break
        else:
            state_code.append(code)
            state_name.append(name)
        if interest_over_time_temp.isnull().values.any():
            interest_over_time_temp = interest_over_time_temp.fillna(interest_over_time_temp.mean())
        decomposition = sm.tsa.seasonal_decompose(interest_over_time_temp, model='additive', extrapolate_trend=1, two_sided=True)
        trend_data = decomposition.trend
        period1_data = trend_data.loc[datelist[0] : datelist[1]] #The idea is to filter the data for year 2018-2019 and make the analysis. Don't know YET how to do this.
        try:
            period1_result = round(mean(period1_data[(int((len(period1_data)+1)/2)):]) / mean(period1_data[:(int((len(period1_data)+1)/2))]) - 1, 2)
        except ZeroDivisionError:
            print('ZeroDivisionError: Data for ' + i + ' is scarce')
            period1_coef.append('NaN')
        else:
            period1_coef.append(period1_result)
            '''
            if period1_result <= parameter[0]:
                period1_change.append('Significant decline')
            elif period1_result <= parameter[1]:
                period1_change.append('Moderate decline')
            elif period1_result < parameter[2]:
                period1_change.append('Stable')
            elif period1_result < parameter[3]:
                period1_change.append('Moderate growth')
            elif period1_result >= parameter[3]:
                period1_change.append('Significant growth')
            '''
        
        period2_data = trend_data.loc[datelist[2] : datelist[3]] #The idea is to filter the data for year 2020-now and make the analysis. Don't know YET how to do this.
        try:
            period2_result = round(mean(period2_data[(int((len(period2_data)+1)/2)):]) / mean(period2_data[:(int((len(period2_data)+1)/2))]) - 1, 2)
        except ZeroDivisionError:
            print('ZeroDivisionError: Data for period 2 of ' + i + ' is scarce')
            period2_coef.append('NaN')
        else:
            period2_coef.append(period2_result)
        '''
            if period2_result <= parameter[0]:
                period2_change.append('Significant decline')
            elif period2_result <= parameter[1]:
                period2_change.append('Moderate decline')
            elif period2_result < parameter[2]:
                period2_change.append('Stable')
            elif period2_result < parameter[3]:
                period2_change.append('Moderate growth')
            elif period2_result >= parameter[3]:
                period2_change.append('Significant growth')
        '''
    dictionary = {'state_code': state_code,
                  'state_name': state_name,
                  'coef1' : period1_coef,
                  'coef2' : period2_coef}
    heatmap = pd.DataFrame(dictionary)
    return heatmap

# def state_heatmap(heatmap_df, datelist):
#     """Create a choropleth image (map) but we are not using thid function for now"""

#     heatmap_df.state_code = heatmap_df.state_code.str.split('-').apply(lambda x: x[1])
#     # Generate the first choropleth for the first period.
#     heatmap_df['coef1'] = heatmap_df['coef1'].apply(lambda x: x*100)
#     fig1 = px.choropleth(heatmap_df,
#                         locations = heatmap_df['state_code'],
#                         locationmode = 'USA-states',
#                         color = heatmap_df['coef1'],
#                         scope = 'usa',
#                         color_continuous_scale = px.colors.diverging.Fall,
#                         color_continuous_midpoint = 0,
#                         labels={'coef1':'% of change'})
#     fig1.write_image('./Graphs/choropleth_period1.png', format='png', engine='kaleido')
#     # Generate the seconde choropleth for the second period.
#     heatmap_df['coef2'] = heatmap_df['coef2'].apply(lambda x: x*100)
#     fig2 = px.choropleth(heatmap_df,
#                         locations = heatmap_df['state_code'],
#                         locationmode = 'USA-states',
#                         color = heatmap_df['coef2'],
#                         scope = 'usa',
#                         color_continuous_scale=px.colors.diverging.Fall,
#                         color_continuous_midpoint = 0,
#                         labels={'coef2': '% of change'})
#     fig2.show()
#     fig2.write_image('./Graphs/choropleth_period2.png', format='png', engine='kaleido')