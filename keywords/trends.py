import requests #To install go to command line and write pip install requests
import hashlib
import sys
import io
from datetime import date, timedelta # To index dates for time series
import pickle # To save and load Pickle files (JSON doesn't store dataframes efficiently)
import time
import os

# Packages for Google Ads functionality.
from google.ads.googleads.client import GoogleAdsClient
from google.ads.googleads.errors import GoogleAdsException
import pandas as pd

# Quant
import numpy as np
import pandas as pd
from pandas import read_fwf
import statsmodels.api as sm # Import Statsmodels to build SARIMA
import pmdarima # To identify te best SARIMA parameters
import itertools # Combination tool to use to find best parameters
import warnings # To avoid SARIMA warnings
import math
import datetime

# Plot & PPTX
import matplotlib.pyplot as plt
from matplotlib.pyplot import scatter, xlabel, title, plot
from pandas.plotting import autocorrelation_plot # Look at autocorrelation graph
from pylab import rcParams # Import tool to decompose time series
import pptx #Python presentation
from PIL import Image # To move figures to PPT
from pptx.util import Inches

# Specific functions separated in other files for readability
from iota.keywords import download_and_load_data
from iota.keywords import keyword_denormalize
from iota.keywords import basic_graphs
from iota.keywords import models
from iota.keywords import heatmaps
from iota.keywords import create_ppt
from iota.keywords import adwords

from django.conf import settings

def generate_report(keywords, service_id):

    workpath = os.path.dirname(os.path.abspath(__file__))

    client = GoogleAdsClient.load_from_storage(os.path.join(workpath, "./google-ads.yaml"))
    customer_id = settings.CUSTOMER_ID

    # Verification of the keywords available in Google Ads and the keyword_plan_id.
    # wordlist_input_partial = ['airdrop', 'alpha']
    wordlist_input_partial = keywords
    wordlist_input = list(map(lambda x:x.lower(),wordlist_input_partial))
    keyword_plan_id, goads_keyword_dict = adwords.add_keyword_plan(client, customer_id, wordlist_input)

    # Wordlist
    values = goads_keyword_dict.values()
    final_wordlist = list(values)
    # CPC list
    cpc_us = adwords.generate_forecast_metrics(client, customer_id, keyword_plan_id, goads_keyword_dict)
    # Search Volume and Competition
    search_volume, competition = adwords.generate_historical_metrics(client, customer_id, keyword_plan_id, goads_keyword_dict)
    # Dataframe creation of Google Ads. This is the main input for the rest of the script.
    keyword_dict = {"Keyword": final_wordlist, "Search Volume (US)": search_volume, "CPC (US)": cpc_us, "Competition (US)": competition}
    keyword_df = pd.DataFrame.from_dict(keyword_dict)
    keyword_volume = keyword_denormalize.keyword_volume(keyword_df)

    word_round = 20
    states = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "District of Columbia", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]

    year_periods = str(datetime.date.today() - datetime.timedelta(weeks=260))
    region_change_time = pd.date_range(year_periods, periods=5, freq="Y")

    date_list = []

    for reg_date in range(len(region_change_time)):
        date_list.append(region_change_time[reg_date].strftime(format="%Y-%m-%d"))

    date_list.append(str(datetime.date.today()))

    wordlist = final_wordlist
    years=4
    sarima_parameters=[1,1,1,1,1,0,52] #Parameters that minimize Akaike Information Criterion (AIC)
    datelist = ['2018-1', '2019-12', '2020-1', '2021-2']
    # The parameters should be in the following order (period1_start, period2_end, period2_start, period2_end). datelist variable can have the format of %Y OR %Y-%m. This slicing is inclusive. This means that the values of the start and ends are included.

    # Load data
    request_dict = download_and_load_data.load_data(wordlist, states, date_list, word_round = 20, input = 1)
    request_dict = keyword_denormalize.de_normalize_words(request_dict, keyword_volume)

    # Heatmap creation for US_dict.
    parameter = [-0.2, -0.1, 0.1, 0.2]

    # In order you must to set the value who need to be < parameter[0], < parameter[1], < parameter[2], < parameter[3], and >= parameter[3]

    keyword_cpc = keyword_volume['CPC (US)']

    interest_over_time_volume = request_dict['interest_over_time_df']
    heatmap_df, wordlist = heatmaps.trend_change_df(wordlist, interest_over_time_volume, datelist, parameter, keyword_df)

    # Before US_dict
    request_dict['heatmap_df'] = heatmap_df

    # Create the ppt for analysis and related queries
    url_report = create_ppt.ppt_report(request_dict, datelist, years, sarima_parameters, wordlist, heatmap_df, word_round, wordlist_input, 1, service_id)

    print("FINISHED")

    return url_report
