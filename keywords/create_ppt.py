'''
This function creates a new PPT with the information of related queries, rela-
ted topics, similar keywords and the graphic created by the data.
'''

from iota.market.models import TrendsReport

# Django
from django.conf import settings
from django.core.mail import EmailMessage
from django.utils import timezone

# import trends reports modules
from iota.keywords import basic_graphs
from iota.keywords import models
from iota.keywords import wordcloudgeneration
from iota.keywords import s3_functionalities
from iota.keywords import heatmaps

# import utilities
import datetime
import dataframe_image as dfi
from pptx import Presentation
from pptx.util import Inches
from PIL import Image
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import numpy as np
import math
import time
import os
import pandas as pd
import pandasql as psql
import matplotlib.pyplot as plt
from io import BytesIO

# Standard
from os import environ
import json
from pptx import Presentation
import io
# AWS
import boto3
# email
import smtplib, ssl
# # tasks
# from iota.celery.tasks import send_email_sync
# services models
from iota.market.models import Service

def ppt_report(request_dict, datelist, years, sarima_parameters, wordlist, heatmap_df, word_round, wordlist_input, show, service_id): #Show - 0 for no display, 1 for display of graphs
    '''
    This function creates the pptx using the previeous stablished template and the graphs 
    generated in basic_graphs, models, heatmap and wordcloudgeneration modules
    '''
    
    # # lines to know the idx of the place holder:
    #     for shape in slidecontent1.placeholders:
    #         print('%d %s' % (shape.placeholder_format.idx, shape.name))
    #         breakpoint()

    # Load template and save to separate file
    bucket_name = 'market-services'
    key_name = 'trends-reports/templates/Google_trends.pptx'
    ppt_template = s3_functionalities.recoveryfilesfroms3(bucket_name, key_name)
    prs = Presentation(ppt_template)
    final_report = BytesIO()
    prs.save(final_report)

    # Create title slide
    title_slide_layout = prs.slide_layouts[0]
    slide = prs.slides.add_slide(title_slide_layout)
    subdate = slide.placeholders[10]
    subdate.text="Last updated "+ str(datetime.date.today())

    # create slide 2 (from 0)
    resume_slide_layout = prs.slide_layouts[1]
    slider = prs.slides.add_slide(resume_slide_layout)
    aux = "\n".join(wordlist_input)
    abstract = slider.placeholders[10]
    abstract.text = aux
    
    # create assumptions slide
    content_slide_layout_hmassumptions = prs.slide_layouts[11]
    assumptions = prs.slides.add_slide(content_slide_layout_hmassumptions)


    # Create heatmap slide
    heatmap_temp = heatmap_df[:]
    for i in range(math.ceil(len(heatmap_df)/word_round)):
        print("procesing the " + str(i + 1) + " heatmap")
        heatmap_image = heatmaps.heatmap_img(heatmap_temp[:word_round])
        time.sleep(1)
        heatmap_slide_layout = prs.slide_layouts[2]
        slidehm = prs.slides.add_slide(heatmap_slide_layout)
        heatmap_title = slidehm.placeholders[0]
        heatmap_title.text="Heatmap Table"
        heatmap_image.seek(0)
        slidehm.shapes.add_picture(heatmap_image, Inches(2.2), Inches(1.7))

    # Create analysis slide
    analysis_slide_layout = prs.slide_layouts[12]
    analysis_slide = prs.slides.add_slide(analysis_slide_layout)
    analysis_title = analysis_slide.placeholders[0]
    analysis_title.text="Heatmap Analysis"
    analysis = analysis_slide.placeholders[10]
    cpc_higer_values_words_str = heatmap_df.sort_values(heatmap_df.columns[1], ascending=False).iloc[:,0].head(1).to_string(header=False, index=False).replace("\n",",").strip()
    cpc_lower_values_words_str = heatmap_df.sort_values(heatmap_df.columns[1]).iloc[:,0].head(1).to_string(header=False, index=False).replace("\n",",").strip()
    significant_growth_last_period_df = psql.sqldf("WITH cte(c0, c1, c2, c3, c4, c5) AS (SELECT * FROM heatmap_df) SELECT c0 FROM cte WHERE c5 = 'Significant growth'")
    significant_growth_last_period_with_spaces = significant_growth_last_period_df.to_string(header=False, index=False).replace("\n",",").strip()
    if significant_growth_last_period_df.empty:
        significant_growth_last_period = "There are no words with significant growth"
    else:
        significant_growth_last_period = " ".join(significant_growth_last_period_with_spaces.split())
    significant_decline_last_period_df = psql.sqldf("WITH cte(c0, c1, c2, c3, c4, c5) AS (SELECT * FROM heatmap_df) SELECT c0 FROM cte WHERE c5 = 'Significant decline'")
    significant_decline_last_period_with_spaces = significant_decline_last_period_df.to_string(header=False, index=False).replace("\n",",").strip()
    if significant_decline_last_period_df.empty:
        significant_decline_last_period = "There are no words with significant decline"
    else:
        significant_decline_last_period = " ".join(significant_decline_last_period_with_spaces.split())
    analysis.text="The word with the highest cost per click: " + cpc_higer_values_words_str + "\n" + "The words with the lowest cost per click: " + cpc_lower_values_words_str + "\n" + "Words with a significant growth in searches in the las period: " + significant_growth_last_period + "\n" + "Words with a significant decline in searches in the last period: " + significant_decline_last_period
    
    # Create slides with Graphics
    for i in wordlist['Keyword']:
        # check if the word has data and if not create the not found slide
        results, plot4 = models.sarima_model(request_dict, i, sarima_parameters, show)
        if results == True:
            notfound_slide_layout = prs.slide_layouts[13]
            notfound_slide = prs.slides.add_slide(notfound_slide_layout)
            notfound_info = slide.placeholders[0]
            notfound_info.text="We couldn't find enough search and trending information about " + i
            continue
        time.sleep(0.5)

        #  slide 3 (from 0)
        content_slide_layout1 = prs.slide_layouts[3]
        slide3 = prs.slides.add_slide(content_slide_layout1)
        slidetitle = slide3.placeholders[0]
        slidetitle.text="Searches per week over past 4 years for " + i
        #Overlay graphs
        plot2 = basic_graphs.overlay_graphs(request_dict, i, years, show)
        time.sleep(0.5)
        plot2.seek(0)
        slide3.shapes.add_picture(plot2, Inches(1.25), Inches(1.25))

        #  slide 4 (from 0)
        content_slide_layout2 = prs.slide_layouts[4]
        slide4 = prs.slides.add_slide(content_slide_layout2)
        slidetitle = slide4.placeholders[0]
        slidetitle.text="Data decomposition for " + i
        # Decompose trend
        plot3 = basic_graphs.decompose_trend(request_dict, i, show)
        time.sleep(0.5)
        # placeholder = slide4.placeholders[10]
        plot3.seek(0)
        slide4.shapes.add_picture(plot3, Inches(1.25), Inches(1.25))

        #  slide 5 (from 0)
        content_slide_layout3 = prs.slide_layouts[5]
        slide5 = prs.slides.add_slide(content_slide_layout3)
        slidetitle = slide5.placeholders[0]
        slidetitle.text="Searches per week overtime for " + i
        # Baseline graphs
        plot1 = basic_graphs.baseline_graphs(request_dict, i, years, show)
        time.sleep(0.5)
        # placeholder = slide5.placeholders[10]
        plot1.seek(0)
        slide5.shapes.add_picture(plot1, Inches(1.75), Inches(1.25))

        #  slide 6 (from 0)
        content_slide_layout4 = prs.slide_layouts[6]
        slide6 = prs.slides.add_slide(content_slide_layout4)
        slidetitle = slide6.placeholders[0]
        slidetitle.text="Forecast data vs Observed data for " + i
        # add plot4
        time.sleep(0.5)
        # placeholder = slide6.placeholders[10]
        plot4.seek(0)
        slide6.shapes.add_picture(plot4, Inches(1.1), Inches(1.25))

    # annexes
    content_slide_layout_annexes = prs.slide_layouts[7]
    annexes = prs.slides.add_slide(content_slide_layout_annexes)

    # tables
    for i in wordlist['Keyword']:
        # check if the word has data 
        results, plot4 = models.sarima_model(request_dict, i, sarima_parameters, show)
        if results == True:
            continue
        time.sleep(0.5)

        # Related Tables
        queries_top_df = request_dict['related_queries_top'][i]
        queries_rising_df = request_dict['related_queries_rising'][i]
        related_topic_df = request_dict['related_topics_top'][i]
        if related_topic_df.empty:
            print('The dataframe for ' + i + ' is empty')
        else:
            related_topic_df = related_topic_df.loc[:, ['topic_title', 'topic_type', 'value']]
            related_topic_df.columns = ['Topic Title', 'Type', 'Value']
        related_topic_rise_df = request_dict['related_topics_rising'][i]
        if related_topic_rise_df.empty:
            print('The dataframe for ' + i + ' is empty')
        else:
            related_topic_rise_df = related_topic_rise_df.loc[:, ['topic_title', 'topic_type', 'value']]
            related_topic_rise_df.columns = ['Topic Title', 'Type', 'Value']
        print(i)

        # slide queries rising and top
        content_slide_layout_relatedqueris = prs.slide_layouts[8]
        relatedqueris = prs.slides.add_slide(content_slide_layout_relatedqueris)
        slidetitle8 = relatedqueris.placeholders[0]
        slidetitle8.text="Related Searches for " + i

        if queries_top_df is not None:
            queries_top_df = queries_top_df.head(3)
            queries_top_df.index += 1
            queries_top_image = BytesIO()
            dfi.export(queries_top_df, queries_top_image)
            queries_top_image.seek(0)
            relatedqueris.shapes.add_picture(queries_top_image, Inches(1.35), Inches(3))

        if queries_rising_df is not None:
            queries_rising_df = queries_rising_df.head(3)
            queries_rising_df.index += 1
            queries_rising_image = BytesIO()
            dfi.export(queries_rising_df, queries_rising_image)
            queries_rising_image.seek(0)
            relatedqueris.shapes.add_picture(queries_rising_image, Inches(7.25), Inches(3))

        # slide topics rising and top
        content_slide_layout_relatedtopics = prs.slide_layouts[9]
        relatedtopics = prs.slides.add_slide(content_slide_layout_relatedtopics)
        slidetitle9 = relatedtopics.placeholders[0]
        slidetitle9.text="Related Searches for " + i

        if related_topic_df is not None:
            related_topic_df = related_topic_df.head(8)
            related_topic_df.index += 1
            related_topic_top_image = BytesIO()
            dfi.export(related_topic_df, related_topic_top_image)
            related_topic_top_image.seek(0)
            relatedtopics.shapes.add_picture(related_topic_top_image, Inches(1.35), Inches(3))

        if related_topic_rise_df is not None:
            related_topic_rise_df = related_topic_rise_df.head(8)
            related_topic_rise_df.index +=1
            related_topic_rise_image = BytesIO()
            dfi.export(related_topic_rise_df, related_topic_rise_image)
            related_topic_rise_image.seek(0)
            relatedtopics.shapes.add_picture(related_topic_rise_image, Inches(7.25), Inches(3))

    #     # Creation of the Choropleth Map
    #     heatmap_df = heatmaps.state_trend_data(request_dict, i, datelist)
    #     try:
    #         heatmaps.state_heatmap(heatmap_df, datelist)
    #     except AttributeError:
    #         print("There is no info for " + i + "by state")
    #     else:
    #         state_hm = prs.slide_layouts[3]
    #         slide = prs.slides.add_slide(state_hm)
    #         title = slide.placeholders[13]
    #         title.text = 'Relative change for \"' + i + '\" from ' + datelist[0] + ' to ' + datelist[1]
    #         _add_image(slide, 14, "../Graphs/choropleth_period1.png")
    #         state_hm = prs.slide_layouts[3]
    #         slide = prs.slides.add_slide(state_hm)
    #         title = slide.placeholders[13]
    #         title.text = 'Relative change for \"' + i + '\" from ' + datelist[2] + ' to ' + datelist[3]
    #         _add_image(slide, 14, "../Graphs/choropleth_period2.png")

    # Create Wordcloud slide
    # Wordcloud creation
    wc_final_text, wcdf = wordcloudgeneration.relatedToText(request_dict, wordlist) # If you want to start a new file select input = 'start', otherwise select input = 'update'
    wordcloud = wordcloudgeneration.wc_plot(wc_final_text)
    # wordcloud slide
    wordcloud_slide = prs.slide_layouts[10]
    wcslide = prs.slides.add_slide(wordcloud_slide)
    wcdf.seek(0)
    wcslide.shapes.add_picture(wcdf, Inches(1), Inches(1.25))
    wordcloud.seek(0)
    wcslide.shapes.add_picture(wordcloud, Inches(6.55), Inches(1.3))
    
    # # datetime
    # now = timezone.now()
    # date_time = now.strftime("%m/%d/%Y-%H:%M:%S")
    AWS_S3_HOST = f'https://market-services.s3.amazonaws.com/'

    # Save report in s3
    prs.save(final_report)
    final_report.seek(0)
    object_name = "trends-reports/reports/google_trends_analysis_" + str(service_id) + ".pptx"
    s3_functionalities.uploadfilestos3(final_report, bucket_name, object_name)
    url_report = AWS_S3_HOST + object_name
    trends_report_object = TrendsReport.objects.get(id=service_id)
    trends_report_object.url = url_report
    trends_report_object.save()

    return url_report