import boto3
import io

def recoveryfilesfroms3(bucket_name, key_name):
    '''recovery files from s3'''

    client = boto3.client(
        's3'
        )

    obj = client.get_object(
        Bucket = bucket_name,
        Key = key_name
        )

    object_content = io.BytesIO(obj['Body'].read())
    return object_content

def uploadfilestos3(file, bucket_name, object_name):
    '''upload file to s3'''
    
    client = boto3.client(
        's3'
        )
    response = None
    with file as fl:
        response = client.upload_fileobj(fl, bucket_name, object_name)
    return response