'''Wordcloud generation'''

# import utilities
import dataframe_image as dfi
from PIL import Image
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import numpy as np
import pandas as pd
from io import BytesIO
from iota.keywords import s3_functionalities

def relatedToText(request_dict, wordlist): 
    '''Input 'start', begin the input creating the relevant files; Input 'update' adds information to an existing file'''

    wc_list = []
    related_queries_top = request_dict['related_queries_top']
    related_queries_rising = request_dict['related_queries_rising']
    related_topics_top = request_dict['related_topics_top']
    related_topics_rising = request_dict['related_topics_rising']
    for i in wordlist['Keyword']:
        related_queries_top_list = []
        related_queries_rising_list = []
        related_topics_top_list = []
        related_topics_rising_list = []
        if related_queries_top[i] is None or related_queries_top[i].empty:
            print('There are no related queries top for ' + i)
        else:
            related_queries_top_list = related_queries_top[i]['query'].tolist()
            wc_list.extend(related_queries_top_list)
        if related_queries_rising[i] is None or related_queries_rising[i].empty:
            print('There are no related emerging queries for ' + i)
        else:
            related_queries_rising_list = related_queries_rising[i]['query'].tolist()
            wc_list.extend(related_queries_rising_list)
        if related_topics_top[i] is None or related_topics_top[i].empty:
            print('There are no related top topics for ' + i)
        else:
            related_topics_top_list = related_topics_top[i]['topic_title'].tolist()
            wc_list.extend(related_topics_top_list)
        if related_topics_rising[i] is None or related_topics_rising[i].empty:
            print('There are no related emerging topics for ' + i)
        else:
            related_topics_rising_list = related_topics_rising[i]['topic_title'].tolist()
            wc_list.extend(related_topics_rising_list)
    wc_text = ' '.join(wc_list)
    wc_text = wc_text.lower()
    wc_final_text = wc_text

    # create wc dataframe
    b = set(wc_list)
    dfwc = pd.DataFrame()
    c = list(b)
    dfwc['Query​'] = c
    e = []
    for i in dfwc.index:
        e.append(wc_list.count((dfwc['Query​'][i])))
    dfwc['Search Frequency​'] = e
    f = []
    for i in dfwc.index:
        f.append((dfwc['Search Frequency​'][i] * 100)/(dfwc['Search Frequency​'].sum()))
    dfwc['Search frequency Percentage (%)'] = f
    dfwcf = dfwc.sort_values('Search frequency Percentage (%)',ascending=False).head(15)
    image_wcdf = BytesIO()
    dfi.export(dfwcf, image_wcdf)
    return wc_final_text, image_wcdf

def wc_plot(wc_final_text):
    '''Generates the wordcloud image'''

    # # recovery word_mask template
    # bucket_name = 'market-services'
    # key_name = 'trends-reports/templates/word_mask.png'
    # word_mask = s3_functionalities.recoveryfilesfroms3(bucket_name, key_name)

    # wordcloud generation
    image_wordcloud = BytesIO()
    stopwords_added = {'love', 'google', 'tim', 'm', 'x', 'pa'}
    stopwords = STOPWORDS | stopwords_added
    # custom_mask = np.array(Image.open(word_mask))
    bucket_name = 'market-services'
    key_name = 'trends-reports/templates/word_mask.png'
    wcmask = s3_functionalities.recoveryfilesfroms3(bucket_name, key_name)
    custom_mask = np.array(Image.open(wcmask))
    wc = WordCloud(background_color='white',
                    scale = 0.4,
                    stopwords = stopwords,
                    mask = custom_mask,
                    repeat = False,
                    max_words=250,
                    collocations=False)
    wc.generate(wc_final_text)
    image_colors = ImageColorGenerator(custom_mask)
    wc.recolor(color_func = image_colors)
    wc.to_image().save(image_wordcloud, 'png')
    return image_wordcloud
