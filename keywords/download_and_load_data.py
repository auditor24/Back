'''Functions for downloading and loading the data'''


# Import packages
import collections
# from utils import mongodb, crypto
from datetime import date, timedelta # To index dates for time series
import numpy as np
import pandas as pd
import pickle # To save and load Pickle files (JSON doesn't store dataframes efficiently)
from pandas import read_fwf
from pytrends.request import TrendReq
import time
import os

def download_trends(wordlist, states, date_list, word_round=20):
    """Download using api and loop to create tables"""
    
    pytrends = TrendReq(hl='en-US', tz=360)
    # Define parameters
    user_cat = 0
    user_timeframe = ['today 5-y', 'today 3-y',
                      'today 12-m', 'today 6-m',
                      'today 3-m']

    search_terms = wordlist[:]
    print(user_cat, user_timeframe[0])
    # Initialize dictionaries
    for_count = 0
    request_dict = {}
    interest_over_time_dic = {}
    interest_over_region_dic = {}
    related_queries_top_dic = {}
    related_queries_rising_dic = {}
    related_topics_top_dic = {}
    related_topics_rise_dic = {}
    for element in search_terms:
        for_count += 1
        pytrends = TrendReq(hl='en-US', tz=360, timeout=(10,25), retries=2, backoff_factor=0.1)
        pytrends.build_payload(kw_list=[element], cat=user_cat, timeframe=user_timeframe[0], geo='US')
        # Relative interest over time.
        interest_over_time = pytrends.interest_over_time()
        interest_over_region = pd.DataFrame(index=states)
        region_df_colums = []
        interest_over_region.index = states
        for i in range(len(date_list)-1):
            # print(i) # Just to keep track of the variable
            pytrends.build_payload(kw_list=[element], cat=user_cat, timeframe=str(date_list[i] + ' ' + date_list[i+1]), geo='US')
            region_df_colums.append(str(date_list[i+1][:-6]))
            interest_over_region[region_df_colums[-1]] = pytrends.interest_by_region()
        # For keywords, related queries, and related topics, it is needed to do it with a timeframe of 3 months.
        pytrends.build_payload(kw_list=[element], cat=user_cat, timeframe=user_timeframe[4], geo='US')
        # Related Queries, returns a dictionary of dataframes.
        related_queries = pytrends.related_queries()
        # Related Topics, returns a dictionary of dataframes.
        related_topics = pytrends.related_topics()
        # Creation of the variables for the dict.
        interest_over_time_dic[element] = interest_over_time
        interest_over_region_dic[element] = interest_over_region
        related_queries_top_dic[element]=related_queries[element]["top"]
        related_queries_rising_dic[element]=related_queries[element]["rising"]
        related_topics_top_dic[element] = related_topics[element]["top"]
        related_topics_rise_dic[element] = related_topics[element]["rising"]
        # Test if there are so many counts for waiting 5 minutes and continue.
        if for_count % word_round == 0:
            print('for_count is equal to ' + str(for_count))
            print('There are too many request. Python will wait 5 minutes to continue')
            time.sleep(300)
            print('Continuing the request...')
        # Update the dictionaries for each word in wordlist.
    request_dict.update({"interest_over_time" : interest_over_time_dic,
                            "interest_over_region": interest_over_region_dic,
                            "related_queries_top" : related_queries_top_dic,
                            "related_queries_rising" : related_queries_rising_dic,
                            "related_topics_top" : related_topics_top_dic,
                            "related_topics_rising" : related_topics_rise_dic # JC: I change this to get the dataframe of related_topics of each word
                       })
    return request_dict

def convert(dict_by_region):
    """Convert dictionaries into datasets for analysis"""

    key_list=list(dict_by_region["interest_over_time"].keys())

    # Convert interest_over_time into dataframe. The first keyword with info is taken into account for creating the data frame.
    for i in range(len(key_list)):
        if not dict_by_region['interest_over_time'][key_list[i]].empty:
            interest_over_time_t=dict_by_region["interest_over_time"][key_list[i]]
            interest_over_time_t=interest_over_time_t.drop("isPartial", axis=1)
            key_list = key_list[i:]
            break
        else:
            continue
    if len(key_list)>1:
        for element in key_list[1:]:
            try:
                interest_over_time_temp=dict_by_region["interest_over_time"][element].drop("isPartial",axis=1)
            except KeyError:
                print(Exception)
                print('Dataframe for ' + element + ' is empty')
            else:
                interest_over_time_temp=dict_by_region["interest_over_time"][element].drop("isPartial",axis=1)
                interest_over_time_t=interest_over_time_t.merge(interest_over_time_temp,on="date",how="outer")

            # Return converted
            converted={}
            converted.update({"interest_over_time":interest_over_time_t, # I droped interest_region because there was no info.
                              "related_queries_top":dict_by_region["related_queries_top"],
                              "related_queries_rising":dict_by_region["related_queries_rising"],
                              "related_topics_top":dict_by_region["related_topics_top"],
                              "related_topics_rising":dict_by_region["related_topics_rising"]
                              })
        return converted


def export_pickle(request_dict):
    """Save dictionary as pickle file"""

    pickle_out = open("../Data/trends.pickle", 'wb')
    pickle.dump(request_dict, pickle_out)
    pickle_out.close()


def load_pickle():
    """Load dictionary from pickle file"""

    pickle_in = open("../Data/trends.pickle", 'rb')
    request_dict = pickle.load(pickle_in)
    return request_dict


def load_data(wordlist, states, date_list, word_round = 20, input=2): #input = 1 for download all data again, 2 for just loading pickle file, 3 to load and append new data
    """Load data"""

    if input==1:
        print("Downloading data from Google and updating")
        request_dict = download_trends(wordlist, states, date_list, word_round = 20)
        # export_pickle(request_dict)
        return request_dict
    if input==2:
        print("Loading data from past download")
        request_dict = load_pickle()
        return request_dict
