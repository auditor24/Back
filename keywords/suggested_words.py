# utilities
import os
import numpy as np
import pandas as pd
import openai # GPT3 package
from dotenv import load_dotenv

# Django
from django.contrib.staticfiles.storage import staticfiles_storage

# Load environment variables
load_dotenv()
openai.api_key = os.environ.get("OPENAI_KEY")

def load_data(self, specified_answers):
    '''
    Parameters: Are suposed to be integers.
    Input: This function expects a training.csv and a testing.csv.
    Optionally you have to give an output.csv and a categories.csv
    '''
    train_file_csv = staticfiles_storage.path('data/keywords/training.csv')
    training_data = pd.read_csv(train_file_csv)
    output = specified_answers
    return training_data, output

def get_response(self, train, output):
    '''
    Parameters: Four databases, train, test, categories and output.
    Output: This returns a dataframe 'output.xlsx' with GPT3 categories.
    '''
    train_copy=train.copy()
    train_copy2 = train_copy.copy()
    train_copy2["Interim"]= ":"
    train_copy2 = train_copy2[["Text", "Interim", "Category"]]
    training_text = train_copy2.values[0:len(train.values)] # Why  [0:len(train.values)]?
    training_text = f"{training_text}"

    charc_for_change = "[]\'\""
    for change in charc_for_change:
        print("ENTRO CORRECTAMENTE") # por probar
        training_text = training_text.replace(change, "")

    training_text=training_text.replace(" \':\' ",": ") # 5
    training_text=training_text.replace("\n ","\n") # 4
    training_text=training_text.replace(".",", ") # 3
    training_text=training_text.replace("-"," ") # 2
    training_text=training_text.replace(":\n",":") # 1
    print(training_text) # por probar

    word = output
    prompt_text = "The following is a list of suggestions according to a keyword\n\n"+str(training_text)+"\n"+str(word)+' :'
    print(prompt_text)
    response = openai.Completion.create(
        engine="davinci",
        prompt=prompt_text,
        temperature=0,
        max_tokens=10,
        top_p=1.0,
        frequency_penalty=0.0,
        presence_penalty=0.0,
        best_of=2,
        stop=["\n"]
    )
    # print("REVISAR POR ACA ---v") # por probar
    print(word + " : " + response["choices"][0]["text"].strip().lower())
    return response["choices"][0]["text"].strip().lower()

def get_keywords(specified_answers):
    
    train, output = LoadingData.load_data(specified_answers)
    return get_response(train, output)
