'''Consume google trends api'''


import datetime # To index dates for time series
import numpy as np
import pandas as pd
from pandas import read_fwf
import itertools # Combination tool to use to find best parameters


def keyword_volume(keyword_df):
    """Import average monthly keywords search volume"""

    keyword_volume = keyword_df
    keyword_volume["Search Volume (US)"] = keyword_volume["Search Volume (US)"].apply(str) ## JC: This line was added to convert the values of the column "Search Volume (US) to str in order to make the function convert them to numeric in line 35.
    keyword_volume = keyword_volume.dropna(axis='columns', how='all')
    keyword_volume["Search Volume (US)"]=pd.to_numeric(keyword_volume["Search Volume (US)"].str.replace(',',''), errors='coerce')
    return keyword_volume


def de_normalize_words(request_dict, keyword_volume):
    """De-normalize words"""

    # Filter data to past 365 days 
    interest_over_time_df = pd.DataFrame()
    for element in request_dict["interest_over_time"].keys():
        if request_dict['interest_over_time'][element].empty:
            print("Data for " + element + " is empty")
            continue
        interest_over_time_s=request_dict["interest_over_time"][element]
        interest_over_time_s["Date"]=interest_over_time_s.index
        interest_over_time_s["Date"]=interest_over_time_s["Date"].apply(lambda x: datetime.datetime.now()-x)
        interest_over_time_s=interest_over_time_s[interest_over_time_s["Date"].apply(lambda x: x.days<365)]
        interest_over_time_s=interest_over_time_s.drop("Date", axis=1)
        # Calculate mean intensity of past 365 days
        interest_over_time_mean=interest_over_time_s.mean(axis = 0).to_frame(name="Mean").reset_index()
        interest_over_time_mean["Keyword"]=interest_over_time_mean["index"].str.lower()
        
        keyword_volume_merge=keyword_volume.merge(interest_over_time_mean,on="Keyword",how="outer")
        keyword_volume_merge["max_volume"]=(keyword_volume_merge["Search Volume (US)"]/(keyword_volume_merge["Mean"]/100))/(52/12)
    
    
    # Assume mean intensity is the monthly search volume of past 365 days and extrapolate to all data
        interest_over_time_volume=request_dict["interest_over_time"][element].drop("Date",axis=1)
        y=list(interest_over_time_volume.columns)
        for i in range(len(y)):
            x=keyword_volume_merge[keyword_volume_merge["Keyword"]==y[i]]["max_volume"]
            if len(x)==0:
                x=100
                print("Warning: multiplier not found")
            else:
                x=x.values[0]
            interest_over_time_volume[element]=interest_over_time_volume[element]*x/100
        if interest_over_time_df.empty:
            interest_over_time_df = interest_over_time_volume.copy()
            interest_over_time_df.drop("isPartial", axis=1, inplace=True)
        else:
            interest_over_time_df[element] = interest_over_time_volume[element]
        if is_unique(interest_over_time_df[element]):
            interest_over_time_df.drop(element, axis=1, inplace=True)
    request_dict["interest_over_time_df"] = interest_over_time_df
    return request_dict


def is_unique(s):
    a = s.to_numpy() # s.values (pandas<0.24)
    return (a[0] == a).all()

