'''Create basic graphs from trends data'''

# Import utilities
import hashlib
import sys
import io
import os
import datetime # To index dates for time series
import numpy as np
import pandas as pd
from pandas import read_fwf
from pandas.plotting import autocorrelation_plot # Look at autocorrelation graph
import matplotlib.pyplot as plt
from matplotlib.pyplot import scatter, xlabel, title, plot
import statsmodels.api as sm # Import Statsmodels to build SARIMA
from pylab import rcParams # Import tool to decompose time series
import itertools # Combination tool to use to find best parameters
from PIL import Image # To move figures to PPT
from io import BytesIO

# correct patplotlib issues
import matplotlib
matplotlib.use('Agg')


### GRAPH DATA FUNCTIONS

def baseline_graphs(request_dict, wordlist, years,show=0):
    """Create and return the baseline graph (plot1)"""
    
    #Create filtered copy of dataset
    converted_updated2=request_dict["interest_over_time_df"].copy()
    converted_updated2["Date"]=converted_updated2.index
    converted_updated2["Date"]=converted_updated2["Date"].apply(lambda x: datetime.datetime.now()-x)
    converted_updated2=converted_updated2[converted_updated2["Date"].apply(lambda x: x.days<years*365)]
    converted_updated2=converted_updated2.drop("Date", axis=1)

    #Initialize plot
    plt.clf()
    image_plot1 = BytesIO()
    plt.figure(figsize=[10, 4.8])
    plt.title("Searches per week over time")
    plt.xlabel("Week")
    plt.ylabel("Estimated searches per week")

    #Graph words in list
    for word in wordlist:
        converted_updated2[wordlist].plot()

    # Plot the function    
    plt.ylim(bottom=0)
    plt.tight_layout()
    plt.gcf()
    plt.savefig(image_plot1)
    return image_plot1 

def overlay_graphs(request_dict, word, years,show=0):
    """Create and return the overlay graph (plot2)"""

    #Create filtered copy of dataset
    converted_updated2=request_dict["interest_over_time_df"].copy()
    converted_updated2["Date"]=converted_updated2.index
    converted_updated2["Date2"]=converted_updated2["Date"].apply(lambda x: datetime.datetime.now()-x)
    
    #Create x week values
    v=converted_updated2[converted_updated2["Date2"].apply(lambda x: x.days<365)]
    x=list(range(1,53))

    #Initialize plot
    plt.clf()
    image_plot2 = BytesIO()
    plt.figure(figsize=[11, 4.8])
    plt.title("Searches per week over past 4 years")
    plt.xlabel("Week (51 = current week, 0 = 51 weeks ago)")
    plt.ylabel("Estimated searches per week")
    
    # Create y lists of values and plot
    counter=0
    y=[]
    while counter<years:
        converted_updated3=converted_updated2.copy()
        converted_updated3=converted_updated3.iloc[len(converted_updated3)-52-counter*52:len(converted_updated3)-counter*52]
        y.append(list(converted_updated3[word].values))
        counter+=1

    # Plot the function
    for i in range(len(y)):
        plt.plot(x,y[i],label = '%s years ago'%i)
    plt.ylim(bottom=0)
    plt.legend()
    plt.gcf()
    plt.savefig(image_plot2)
    return image_plot2

def decompose_trend(request_dict, word, show=0):
    """Create and return the decompose trend graph (plot3)"""

    #Create filtered copy of dataset
    converted_updated2=request_dict["interest_over_time_df"].copy()
    y=converted_updated2[word]
    
    # Initialize plot
    image_plot3 = BytesIO()
    
    # plot the function
    decomposition = sm.tsa.seasonal_decompose(y, model='additive', extrapolate_trend=1, two_sided=True)
    fig3 = decomposition.plot()
    fig3.set_size_inches(11, 4.8)
    # fig3.figure(figsize=[11, 4.8])
    fig3.savefig(image_plot3)
    return image_plot3
