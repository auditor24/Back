'''Create plot from lialized data'''

# Import packages
import hashlib
import sys
import io
import datetime # To index dates for time series
import numpy as np
import pandas as pd
import pickle # To save and load Pickle files (JSON doesn't store dataframes efficiently)
from pandas import read_fwf
# from IPython.display import display, Markdown # For pretty-printing tibbles
import matplotlib.pyplot as plt
from matplotlib.pyplot import scatter, xlabel, title, plot
from pandas.plotting import autocorrelation_plot # Look at autocorrelation graph
import statsmodels.api as sm # Import Statsmodels to build SARIMA
from pylab import rcParams # Import tool to decompose time series
import itertools # Combination tool to use to find best parameters
import warnings # To avoid SARIMA warnings
from io import BytesIO


def sarima_model(request_dict, word, parameters, show=0):
    """create model"""

    #Create filtered copy of dataset
    plt.clf()
    converted_updated2 = request_dict["interest_over_time_df"].copy()
    y=converted_updated2[word]
    all_value_NaN = y.isnull().values.all()
    if all_value_NaN == True:
        return(all_value_NaN)
    #Run model with optimized parameters
    mod = sm.tsa.statespace.SARIMAX(y,order=(parameters[0],parameters[1],parameters[2]),seasonal_order=(parameters[3],parameters[4],parameters[5],parameters[6]),enforce_stationarity=False,enforce_invertibility=False, freq=y.index.inferred_freq)
    results = mod.fit()
    
    # # plot7
    # # Initialize plot
    # plt.clf()
    # image_plot7 = BytesIO()
    # # plot the function
    # plt.rc('figure', figsize=(11, 4.8))
    # plt.text(0.01, 0.05, str(results.summary().tables[1]), {'fontsize': 10}, fontproperties = 'monospace') # approach improved by OP -> monospace!
    # plt.axis('off')
    # plt.tight_layout()
    # plt.savefig(image_plot7)
    
    # # Plot6
    # # Initialize plot
    # plt.clf()
    # image_plot6 = BytesIO()
    # # plot the function
    # x=results.plot_diagnostics(figsize=(18, 8))
    # fig6=plt.gcf()
    # fig6.savefig(image_plot6)
    
    # Prediction - plot4
    # # Initialize plot
    plt.clf()
    image_plot4 = BytesIO()
    # plot the function
    pred = results.get_prediction(start=pd.to_datetime('2020-04-05'), dynamic=False)
    pred_ci = pred.conf_int()
    ax = y['2020':].plot(label='observed')
    pred.predicted_mean.plot(ax=ax, label='One-step ahead Forecast', alpha=.7, figsize=(11, 5))
    ax.fill_between(pred_ci.index,
                    pred_ci.iloc[:, 0],
                    pred_ci.iloc[:, 1], color='k', alpha=.2)
    ax.set_xlabel('Date')
    ax.set_ylabel('Searches')
    plt.legend()
    plt.gcf()
    plt.savefig(image_plot4)

    # # Accuracy measure
    # y_forecasted = pred.predicted_mean
    # y_truth = y['2020-04-26':]
    # mse = ((y_forecasted - y_truth) ** 2).mean()
    # print('The Mean Squared Error is {}'.format(round(mse, 2)))
    # print('The Root Mean Squared Error is {}'.format(round(np.sqrt(mse), 2)))

    # # Prediction - plot5
    # # Initialize plot
    # plt.clf()
    # image_plot5 = BytesIO()
    # # # plot the function
    # pred_uc = results.get_forecast(steps=52)
    # pred_ci = pred_uc.conf_int()
    # ax = y.plot(label='observed', figsize=(14, 4))
    # pred_uc.predicted_mean.plot(ax=ax, label='Forecast')
    # ax.fill_between(pred_ci.index,
    #                 pred_ci.iloc[:, 0],
    #                 pred_ci.iloc[:, 1], color='k', alpha=.25)
    # ax.set_xlabel('Date')
    # ax.set_ylabel('Searches')
    # plt.legend()
    # fig5=plt.gcf()
    # fig5.savefig(image_plot5)

    # y_forecasted = pred.predicted_mean
    # y_forecasted.head(12)
    # y_truth.head(12)
    # pred_ci.head(24)
    # forecast = pred_uc.predicted_mean
    # forecast.head(12)
    
    return results, image_plot4
