'''consume google ads api'''


from google.ads.googleads.client import GoogleAdsClient
from google.ads.googleads.errors import GoogleAdsException
import uuid

def add_keyword_plan(client, customer_id, wordlist):
    """
    Adds a keyword plan, campaign, ad group, etc. to the customer account.
    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.
    Raises: GoogleAdsException: If an error is returned from the API.
    """
    keyword_plan, keyword_plan_id = create_keyword_plan(client, customer_id)
    keyword_plan_campaign = create_keyword_plan_campaign(client, customer_id, keyword_plan)
    keyword_plan_ad_group = create_keyword_plan_ad_group(client, customer_id, keyword_plan_campaign)
    goads_keyword_dict =  create_keyword_plan_ad_group_keywords(client, customer_id, keyword_plan_ad_group, wordlist)

    return keyword_plan_id, goads_keyword_dict


def create_keyword_plan(client, customer_id):
    """
    Adds a keyword plan to the given customer account.
    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.
    Returns: A str of the resource_name for the newly created keyword plan.
    Raises: GoogleAdsException: If an error is returned from the API.
    """
    keyword_plan_service = client.get_service("KeywordPlanService")
    operation = client.get_type("KeywordPlanOperation")
    keyword_plan = operation.create
    keyword_plan.name = f"Keyword plan for traffic estimate {uuid.uuid4()}"
    forecast_interval = (client.enums.KeywordPlanForecastIntervalEnum.NEXT_QUARTER)
    keyword_plan.forecast_period.date_interval = forecast_interval
    response = keyword_plan_service.mutate_keyword_plans(customer_id=customer_id, operations=[operation])
    resource_name = response.results[0].resource_name
    keyword_plan_id = resource_name.split('/')[-1]

    print(f"Created keyword plan with resource name: {resource_name}")
    return resource_name, keyword_plan_id


def create_keyword_plan_campaign(client, customer_id, keyword_plan):
    """
    Adds a keyword plan campaign to the given keyword plan.
    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.
        keyword_plan: A str of the keyword plan resource_name this keyword plan campaign should be attributed to.create_keyword_plan.
    Returns: A str of the resource_name for the newly created keyword plan campaign.
    Raises: GoogleAdsException: If an error is returned from the API.
    """
    keyword_plan_campaign_service = client.get_service("KeywordPlanCampaignService")
    operation = client.get_type("KeywordPlanCampaignOperation")
    keyword_plan_campaign = operation.create
    keyword_plan_campaign.name = f"Keyword plan campaign {uuid.uuid4()}"
    keyword_plan_campaign.cpc_bid_micros = 1000000
    keyword_plan_campaign.keyword_plan = keyword_plan # keyword_plan es un string (e.g.: "customers/8622812517/keywordPlans/341268452")
    network = client.enums.KeywordPlanNetworkEnum.GOOGLE_SEARCH
    keyword_plan_campaign.keyword_plan_network = network
    geo_target = client.get_type("KeywordPlanGeoTarget")
    # Constant for U.S. Other geo target constants can be referenced here:
    # https://developers.google.com/google-ads/api/reference/data/geotargets
    geo_target.geo_target_constant = "geoTargetConstants/2840"
    keyword_plan_campaign.geo_targets.append(geo_target)
    # Constant for English
    language = "languageConstants/1000"
    keyword_plan_campaign.language_constants.append(language)
    response = keyword_plan_campaign_service.mutate_keyword_plan_campaigns(customer_id=customer_id, operations=[operation])
    resource_name = response.results[0].resource_name

    print(f"Created keyword plan campaign with resource name: {resource_name}")
    return resource_name


def create_keyword_plan_ad_group(client, customer_id, keyword_plan_campaign):
    """
    Adds a keyword plan ad group to the given keyword plan campaign.
    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.
        keyword_plan_campaign: A str of the keyword plan campaign resource_name this keyword plan ad group should be attributed to.
    Returns:
        A str of the resource_name for the newly created keyword plan ad group.
    Raises: GoogleAdsException: If an error is returned from the API.
    """
    operation = client.get_type("KeywordPlanAdGroupOperation")
    keyword_plan_ad_group = operation.create
    keyword_plan_ad_group.name = f"Keyword plan ad group {uuid.uuid4()}"
    keyword_plan_ad_group.cpc_bid_micros = 1000000
    keyword_plan_ad_group.keyword_plan_campaign = keyword_plan_campaign
    keyword_plan_ad_group_service = client.get_service("KeywordPlanAdGroupService")
    response = keyword_plan_ad_group_service.mutate_keyword_plan_ad_groups(customer_id=customer_id, operations=[operation])
    resource_name = response.results[0].resource_name

    print(f"Created keyword plan ad group with resource name: {resource_name}")
    return resource_name


def create_keyword_plan_ad_group_keywords(client, customer_id, plan_ad_group, wordlist):
    """
    Adds keyword plan ad group keywords to the given keyword plan ad group.
    Args:
        client: An initialized instance of GoogleAdsClient.
        customer_id: A str of the customer_id to use in requests.
        plan_ad_group: A str of the keyword plan ad group resource_name these keyword plan keywords should be attributed to.
    Raises: GoogleAdsException: If an error is returned from the API.
    """
    keyword_plan_ad_group_keyword_service = client.get_service("KeywordPlanAdGroupKeywordService")
    operation = client.get_type("KeywordPlanAdGroupKeywordOperation")
    operations = []

    for word in wordlist:
        operation = client.get_type("KeywordPlanAdGroupKeywordOperation")
        keyword_plan_ad_group_keyword = operation.create
        keyword_plan_ad_group_keyword.text = word
        keyword_plan_ad_group_keyword.match_type = (client.enums.KeywordMatchTypeEnum.BROAD)
        keyword_plan_ad_group_keyword.keyword_plan_ad_group = plan_ad_group
        operations.append(operation)

    response = keyword_plan_ad_group_keyword_service.mutate_keyword_plan_ad_group_keywords(customer_id=customer_id, operations=operations)
    goads_keyword_dict = {}
    initial_path = "customers/8622812517/keywordPlanAdGroupKeywords/"

    for result in response.results:
        # In this step I create a dict that stores the information about keyword id and keyword value.
        result_keyword_id = result.resource_name.split('/')[-1]
        word = keyword_plan_ad_group_keyword_service.get_keyword_plan_ad_group_keyword(resource_name=initial_path + result_keyword_id).text
        goads_keyword_dict[result_keyword_id] = word

        print("Created keyword plan ad group keyword with resource name: " f"{result.resource_name}")

    return goads_keyword_dict


def generate_forecast_metrics(client, customer_id, keyword_plan_id, goads_keyword_dict):
    '''
    Generate CPC (cost per click) for every keyword passed as input in the keyword plan.
    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.
        keyword_plan_id: An integer of the id for the keyword plan. In the keyword plan is where the keywords are stored and waiting for a request to retrieve data.
        goads_keyword_dict: A dict where the keys are the keyword_id and the value are the string of the keywords requested.
    Returns:
        An integer for CPC (cost per click) metrics.
    '''
    keyword_plan_service = client.get_service("KeywordPlanService")
    resource_name = keyword_plan_service.keyword_plan_path(customer_id, keyword_plan_id)
    response = keyword_plan_service.generate_forecast_metrics(keyword_plan=resource_name)
    cpc_us = []

    for i, forecast in enumerate(response.keyword_forecasts):
        
        print(f"#{i+1} Keyword ID: {forecast.keyword_plan_ad_group_keyword.split('/')[-1]}" + ". The keyword is equal to: " + goads_keyword_dict[forecast.keyword_plan_ad_group_keyword.split('/')[-1]])

        metrics = forecast.keyword_forecast

        cpc = round(metrics.average_cpc/1000000, 2)
        if cpc:
            cpc = cpc
        else:
            cpc = 0.01
        print(f"Estimated average cpc: {cpc}\n")
        cpc_us.append(cpc)
    print(cpc_us)
    return cpc_us
        # [END generate_forecast_metrics]


def generate_historical_metrics(client, customer_id, keyword_plan_id, goads_keyword_dict):
    '''
    Generate search volume and competition metrics for every keyword passed as input in the keyword plan.
    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.
        keyword_plan_id: An integer of the id for the keyword plan. In the keyword plan is where the keywords are stored and waiting for a request to retrieve data.
        goads_keyword_dict: A dict where the keys are the keyword_id and the value are the string of the keywords requested.
    Returns:
        Two lists:
            search_volume: An integer of the number of searches for each keyword.
            competition: An integer of from 1 to 100 where each keyword shows the relative ranking in google searches.
    '''
    keyword_plan_service = client.get_service("KeywordPlanService")
    resource_name = keyword_plan_service.keyword_plan_path(customer_id, keyword_plan_id)
    response = keyword_plan_service.generate_historical_metrics(keyword_plan=resource_name)
    search_volume = []
    competition = []
    count = 0
    for value in goads_keyword_dict.values():
        if value == response.metrics[count].search_query:
            metrics = response.metrics[count].keyword_metrics
            avg_monthly_searches = metrics.avg_monthly_searches
            if avg_monthly_searches:
                if avg_monthly_searches == 0:
                    avg_monthly_searches = 50
            else:
                avg_monthly_searches = 50
            print(f"Estimated average monthly searches: {avg_monthly_searches}")
            competition_index = metrics.competition_index
            if competition_index:
                if competition_index == 0:
                    competition_index = 1
            else:
                competition_index = 1
            print(f"Estimated average competition index: {competition_index}\n")
            search_volume.append(avg_monthly_searches)
            competition.append(competition_index)
            count += 1
        else:
            search_volume.append(50)
            competition.append(1)
        
    return search_volume, competition
